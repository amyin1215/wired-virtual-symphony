package org.lelaboratoire.BluetoothCoffee;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.UUID;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

public class BTClass {
	private BluetoothAdapter mBluetoothAdapter;
	private BluetoothSocket btSocket = null;
	private OutputStream outStream = null;
	private InputStream inputStream = null;
	private static final boolean D = true;

    // Intent request codes
    public static final int REQUEST_CONNECT_DEVICE = 1;
	public final static int REQUEST_ENABLE_BT = 2;
	
	// Well known SPP UUID (will *probably* map to
	// RFCOMM channel 1 (default) if not in use);
	// see comments in onResume().
	private static final UUID MY_UUID = 
			UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
	
	private static final String TAG = "THINBTCLIENT";
	
	public static boolean btEnabled = false;
	public static boolean btConnected = false;

	public void checkAvailability(Activity activity) {
		mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
		if (mBluetoothAdapter == null) {
            Toast.makeText(activity, "Bluetooth is not available", Toast.LENGTH_LONG).show();
			return;
		}

		if (!mBluetoothAdapter.isEnabled()) {
			// Bluetooth is disabled, so request enable
			// Upon return, will refocus on application
		    Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
		    activity.startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
		} else {
			btEnabled = true;
		}
	}
	
	public void startConnection(String address, Activity activity) {
		// When this returns, it will 'know' about the server,
		// via it's MAC address.
		BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(address);
		Log.e(TAG, "startConnection. address: " + address);

		// We need two things before we can successfully connect
		// (authentication issues aside): a MAC address, which we
		// already have, and an RFCOMM channel.
		// Because RFCOMM channels (aka ports) are limited in
		// number, Android doesn't allow you to use them directly;
		// instead you request a RFCOMM mapping based on a service
		// ID. In our case, we will use the well-known SPP Service
		// ID. This ID is in UUID (GUID to you Microsofties)
		// format. Given the UUID, Android will handle the
		// mapping for you. Generally, this will return RFCOMM 1,
		// but not always; it depends what other BlueTooth services
		// are in use on your Android device.
		try {
			btSocket = device.createRfcommSocketToServiceRecord(MY_UUID);
			//Log.e()
		} catch (IOException e) {
			if (D) Log.e(TAG, "ON RESUME: Socket creation failed.", e);
		}

		// Discovery may be going on, e.g., if you're running a
		// 'scan for devices' search from your handset's Bluetooth
		// settings, so we call cancelDiscovery(). Discovery is a
		// heavyweight process; you don't want it in progress when
		// a connection attempt is made.
		mBluetoothAdapter.cancelDiscovery();

		// Blocking connect, for a simple client nothing else can
		// happen until a successful connection is made, so we
		// don't care if it blocks.
		try {
			btSocket.connect();
			if (D) Log.e(TAG, "ON RESUME: BT connection established, data transfer link open.");
		} catch (IOException e) {
			Toast.makeText(activity, "Bluetooth socket closed; IO exception", Toast.LENGTH_SHORT).show();
			try {
				btSocket.close();
			} catch (IOException e2) {
				if (D) Log.e(TAG, 
					"ON RESUME: Unable to close socket during connection failure", e2);				
			}
			return;
		}
		btConnected = true;
		// Turn on LED when connected
		sendDataByte((byte)(OPhoneClass.BTLED + OPhoneClass.ON));
	}
	
	/* Function to test Bluetooth Connection when device selected */
	public boolean sendData(String message) {
		String s = "";
		for (int i = 0; i < message.length(); i++) {
			s += Integer.toString((int)(message.charAt(i))) + " "; 
		}
		if (D) Log.e(TAG, "Sending Data message: " + s);
		try {
			outStream = btSocket.getOutputStream();
		} catch (IOException e) {
			if (D) Log.e(TAG, "sendData: Output stream creation failed.", e);
			return false;
		}

		byte[] msgBuffer = message.getBytes();
		try {
			outStream.write(msgBuffer);
			if (D) Log.e(TAG, "message Buffer: " + msgBuffer + "; length = " + Integer.toString(msgBuffer.length)) ;
		} catch (IOException e) {
			if (D) Log.e(TAG, "sendData: Exception during write.", e);
			return false;
		}
		return true;
	}
	
	// Sends only one byte
	public boolean sendDataByte(byte msgByte) { 
		if (D) {
			if (!btConnected) Log.e(TAG, "Btconnected is false");
			if (!btEnabled) Log.e(TAG, "BtEnabled is false");
		}
		if (btEnabled && btConnected) {
			Log.e(TAG, "btEnabled is true; sendDataByte msgByte= " + msgByte);
			try {
				outStream = btSocket.getOutputStream();
			} catch (IOException e) {
				if (D) Log.e(TAG, "ON RESUME: Output stream creation failed.", e);
				return false;
			}
	
			try {
				outStream.write(msgByte);
			} catch (IOException e) {
				if (D) Log.e(TAG, "ON RESUME: Exception during write.", e);
				return false;
			}
			return true;
		}
		return false;
	}	
	
	public byte[] receiveData() {
		if (!btConnected) {
			if (D) Log.e(TAG, "btConnected is false in receiveData");
			return null;
		}
		try {
			inputStream = btSocket.getInputStream();
		} catch (IOException e) {
			if (D) Log.e(TAG, "ON RESUME: Input stream creation failed.", e);
		}
		byte[] msg = null;
		try {
			int l = inputStream.available();
			msg = new byte[l];
			inputStream.read(msg);
		} catch (IOException e) {
			if (D) Log.e(TAG, "ON RESUME: Exception during read.", e);
		}
		return msg;
	}
	
	
	public void pauseConnection(OPhoneClass oPhone) {
		if (btEnabled && btConnected) {
			oPhone.turnOffAllComponents();
			if (outStream != null) {
				try {
					outStream.flush();
				} catch (IOException e) {
					if (D) Log.e(TAG, "ON PAUSE: Couldn't flush output stream.", e);
				}
			}
	
			try	{
				btSocket.close();
			} catch (IOException e2) {
				if (D) Log.e(TAG, "ON PAUSE: Unable to close socket.", e2);
			}
			btConnected = false;
		}
	}
}


