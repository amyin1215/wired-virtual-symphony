package org.lelaboratoire.BluetoothCoffee;

import android.util.Log;

/* There needs to be one shared timer for all the peltiers */
public class Motor {
	private static final boolean D = true;
	public int scentPos = -1; // how many increments of 50 away from closed position of -1
	Fan[] fanList;
	private static final String TAG = "Peltier Class";
	int id;
	OPhoneClass oPhone;
	
	public Motor(int id, OPhoneClass oPhone){
		this.id = id;
		this.oPhone = oPhone;
	}

	boolean setPosition(int scent) {
		if (scent > OPhoneClass.SCENTSCOUNT || scent < 0) {
			if (D) Log.e(TAG, "Motor " + id + " received invalid scent id = " + scent);
			return false;
		}
		scentPos = scent;
		if (D) Log.e(TAG, "Motor " + id + " setPosition called");
		return oPhone.sendMessage((byte)calcCommand(scent));
	}
	
	int calcCommand(int scent) {
		if (scent == scentPos) { // no movement needed
			return 0;
		}
		if (scent == -1) { // reset back to empty
			return scentPos^5; // XOR 0b0100
		}
		if (scentPos == -1) { 
			return id*16+OPhoneClass.MOT0 + OPhoneClass.SCENTS[scent];		
		} else { // must calculate new number of steps required
			int interval = scentPos - scent; // number of steps needed to get to new position
			if (interval <= 0) { // move in negative direction
				return id*16+OPhoneClass.MOT0 + OPhoneClass.NEG_DIR + Math.abs(interval);
			} else {
				return id*16+OPhoneClass.MOT0 + OPhoneClass.POS_DIR + Math.abs(interval);
			}
		}
	}
	
	boolean resetPosition() {
		// take current position and flip direction to move back to empty
		boolean isSuccess = oPhone.sendMessage((byte)(id*16+OPhoneClass.MOT0 + calcCommand(-1)));	
		scentPos = -1;
		return isSuccess;
	}
	
	/* turns off blower while simultaneously resetting motor position to empty */
	public void turnOff() {
		resetPosition();
		oPhone.sendMessage((byte)(id*16+OPhoneClass.MOT0 + OPhoneClass.OFF));
	}

}