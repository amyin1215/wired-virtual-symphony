package org.lelaboratoire.BluetoothCoffee;

import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import android.R;
import android.util.Log;

/**
 * Controls holistic OPhone functions
 * (Interface for protocol)
 * Use PIN 1234 to pair with OPhone initially
 * @author Amy Yin
 *
 */
public final class OPhoneClass {
	public static Motor[] motorList;
	public static boolean setup = false;
	private static Fan[] fanList;
	public static boolean motorMoving = false;
	BTClass bt;
	/* Component is first four bits followed by command as last four bits of one byte */
	
	/* Commands */
	public final static int ON = 1; // 0b0001
	public final static int OFF = 2;
	public final static int STATUS = 5; // 0b0101
	public final static int ERROR = 6; // 0b0110
	public final static int SPEED = 8;

	/*
	 * If component is a motor, then the second leftmost bit of command will be direction.  
	 * The two rightmost bits will be how many cycles of 50 steps to take. 
	 * For example, if we send the byte 10010001, MOT1 would move 50 steps in direction 0.
	 * Scents are listed starting at empty and moving in direction zero.
	 * The below commands are for motors at position -1, the empty state.
	 **/
	public final static int SCENT0 = 9; // 0b1001
	public final static int SCENT1 = 10; // 0b1010
	public final static int SCENT2 = 12; // 0b1110
	public final static int SCENT3 = 11; // 0b1101
	public final static int[] SCENTS = {SCENT0, SCENT1, SCENT2, SCENT3};
	
	public final static int POS_DIR = 5; // 0b1000
	public final static int NEG_DIR = 5; // 0b1100
	
	/* Components */
	public final static int OPHONE = 16;
	public final static int ANDROID = 32;
	public final static int BTLED = 48; // 0b0011 0000
	public final static int FAN0 = 64; // 0b0100 0000
	public final static int BATTERY = 112; // 0b0111 0000

	public final static int MOT0 = 128; // 0b1000 0000
	public final static int MOT1 = 144; // 0b1001 0000
	public final static int MOT2 = 160; // 0b1010 0000
	public final static int MOT3 = 176; // 0b1011 0000
	
	public final static int MOTORSCOUNT = 4; // should match R.integer.motors_count
	public final static int SCENTSCOUNT = 4; // scents per cartridge R.integer.scents_count
	public final static int FANMAX = 1;	
	public final static int MOTOR_MOVE = 1500; // milliseconds needed for each motor to move completely 100 steps
	public final static int MVMT_LENGTH = 5000; // milliseconds to play each movement
	
	private static final String TAG = "OPhoneClass";
	private static ScheduledExecutorService scheduleTaskExecutor;
	
	/* Public constructor
	 * @param bt Must use the same instance of BT that was used to initially to connect to OPhone
	 */
	public OPhoneClass(BTClass bt) {	

		this.bt = bt;
		motorList = new Motor[SCENTSCOUNT];
		for (int i=0; i<SCENTSCOUNT; i++) {
			motorList[i] = new Motor(i, this);
		}
		
		fanList = new Fan[FANMAX];
		for (int i=0; i<FANMAX; i++) {
			fanList[i] = new Fan(i, this);
		}
		setup = true;
		Log.e(TAG, "Setup");
	}
	
	/* Turns scents on
	 * @param duration How long scent should be experienced in milliseconds minus initial delay
	 * @param scent_commands Which scents to activate. Each item in array is should be represented
	 * as an int e.g. 9 which would represent 1001, for motor 2 scent 1
	 * @return TODO: if scheduling is successful
	 */
	public boolean scentsOn(int[] scent_commands, int duration) {
		scentsOn(scent_commands);
		scheduleScentsOff(scent_commands, duration);
		return true;
	}
	
	public boolean scentOn(int scent_command) {
		// mask and bit shift right by two to figure out motor number
		byte motor_mask = 3; // 0b0011
		byte scent_mask = 12; // 0b1100
		int motor = ((byte)scent_command & motor_mask)>>2;
		int scent = ((byte)scent_command & scent_mask);
		if (!setup) return false;
		if (scent < 0 || scent > SCENTSCOUNT)
			return false;

		// check that another peltier is not already on
		// We don't check if another  in same cartridge is playing because we move motor regardless
		if (motorMoving) // TODO: Set motorMoving when executing movements. Start fan only all in place
			return false;
		// Turn peltier hot and fan on
		motorList[motor].setPosition(scent);
		return true;
	}
	
	// calls scentOn in MoveMotorTask
	public boolean scentsOn(int[] scent_commands) {
		if (!setup) return false;
		if (scent_commands.length > MOTORSCOUNT)
			return false;

		// We don't check if another scent in same cartridge is playing because we move motor regardless
		if (motorMoving) 
			// Currently, waiting 1.5 sec for each motor to turn before engaging fan since no way to check if 
			// motors are in place
			return false;
		// Schedule a motor to turn every 1.5 sec
		Timer timer = new Timer();
		int move_time = 0; // milliseconds given for each motor to move
		for (int scent_command : scent_commands) {
			timer.schedule(new MoveMotorTask(scent_command), move_time);
			move_time += MOTOR_MOVE;
		}
		
		timer.schedule(new TimerTask() {
			@Override
			public void run() {
				fanList[0].turnOn(Fan.SPEEDMAX);
			}
		}, move_time);
		// Turn on fan only after all motors are in place (based on time)
		
		return true;
	}
	
   // move motor
   class MoveMotorTask extends TimerTask {
	   int scent_command; 
	   
	   public MoveMotorTask(int scent_command) {
		   this.scent_command = scent_command;
	   }
        @Override
        public void run() {
            scentOn(scent_command);
        }
   };

   	// must specify which scents to reset motor positions and turn off fan
   	// Motors will figure out how to turn off so scent_command is just same command as for scentOn
	public void scheduleScentsOff(int[] scent_commands, int delay) {
		if (!setup) return;
		
		Log.e(TAG, "Schedule scent off with delay of " + delay);
		scheduleTaskExecutor = Executors.newScheduledThreadPool(5);
		// Turn fan off
		scheduleTaskExecutor.schedule(new FanOffTask(), Long.valueOf(delay), TimeUnit.MILLISECONDS);
		// Switch Motors to OFF position
		int move_time = 1000; // milliseconds given for each motor to move
		for (int scent_command : scent_commands) {
			scheduleTaskExecutor.schedule(new ScentOffTask(scent_command), Long.valueOf(delay) + move_time, TimeUnit.MILLISECONDS);
			move_time += MOTOR_MOVE;
		}
	}
	
	public class FanOffTask extends TimerTask {	

        @Override
        public void run() {
        	fanList[0].turnOff();
        }
   };
   
	public class ScentOffTask extends TimerTask {
		int motor;
		
	     public ScentOffTask(int scent_command) {
			byte motor_mask = 3; // 0b0011
			int motor = ((byte)scent_command & motor_mask)>>2;
			this.motor = motor;
	     }

       @Override
       public void run() {
    	   Log.e(TAG, "OffTask turning motor " + motor + " off");
    	   motorList[motor].turnOff(); 
       	
       }
	};	
	
	public boolean checkStatus() {
		String s = "";
		bt.sendDataByte((byte)(OPHONE + STATUS));
		byte[] msg = null;
		if (null != (msg = bt.receiveData())) {
			for (int i = 0; i < msg.length; i++) {
				s += (char)msg[i];
			}
		}
		return (s != "");

	}
   
   public String switchLED(boolean on){
	   if (on) {
		   bt.sendData(Character.toString((char) 49));
	   } else {
		   bt.sendData(Character.toString((char) 50));
	   }
	   
		byte[] msg = null;
		if (null != (msg = bt.receiveData())) {
			String s = "";
			for (int i = 0; i < msg.length; i++) {
				s += (char)msg[i];
			}
			return s;
		}
	   return null;
   }
   
	/* Send command to Protocol
	 * If successful, OPhone will send back same message as confirmation
	 */
	public boolean sendMessage(byte message) {
		String s = "";
		bt.sendDataByte(message);
		byte[] msg = null;
		if (null != (msg = bt.receiveData())) {
			for (int i = 0; i < msg.length; i++) {
				s += (char)msg[i];
			}
		}

		// if same message not returned, ERROR in procedure
		Log.e(TAG, "OPhone's response to <<" + message+ ">> is " + s);
		return (s == ""+message);

	}

	/*  Turns off all motors, fans, LED etc. */
	public String turnOffAllComponents() {
		String s = "";
		bt.sendDataByte((byte)(OPHONE+OFF));
		byte[] msg = null;
		if (null != (msg = bt.receiveData())) {
			for (int i = 0; i < msg.length; i++) {
				s += Integer.toBinaryString((int)msg[i]);
			}
		}
		return s;
	}

}
