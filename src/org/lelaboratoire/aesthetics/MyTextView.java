package org.lelaboratoire.aesthetics;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

public class MyTextView extends TextView {

    public MyTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public MyTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public MyTextView(Context context) {
        super(context);
        init();
    }

    private void init() {
        if (!isInEditMode()) {
            // get standard font for application
            TypefaceSpan span = new TypefaceSpan(getContext());
            Typeface tf = span.getTypeface(getContext());
            setTypeface(tf);
        }
        setTextSize(36);
    }

}