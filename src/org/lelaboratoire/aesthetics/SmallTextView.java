package org.lelaboratoire.aesthetics;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

public class SmallTextView extends TextView {

    public SmallTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public SmallTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public SmallTextView(Context context) {
        super(context);
        init();
    }

    private void init() {
        if (!isInEditMode()) {
            // get standard font for application
            TypefaceSpan span = new TypefaceSpan(getContext());
            Typeface tf = span.getTypeface(getContext());
            setTypeface(tf);
        }
        setTextSize(17);
    }

}