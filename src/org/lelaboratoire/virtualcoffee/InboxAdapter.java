package org.lelaboratoire.virtualcoffee;
 
import java.util.ArrayList;
import java.util.HashMap;
 
import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
 
public class InboxAdapter extends BaseAdapter {
 
    private Activity activity;
    public ArrayList<HashMap<String, String>> data;
    private static LayoutInflater inflater=null;
    private long id;
 
    public InboxAdapter(Activity a, ArrayList<HashMap<String, String>> d) {
        activity = a;
        data=d;
        inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
 
    public int getCount() {
        return data.size();
    }
 
    public Object getItem(int position) {
        return data.get(position);
    }
 
    public long getItemId(int position) {
        return id;
    }
 
    public View getView(int position, View convertView, ViewGroup parent) {
        View vi=convertView;
        if(convertView==null)
            vi = inflater.inflate(R.layout.list_row, null);
        switch(position % 4) {
	        case 0:vi.setBackgroundResource(R.drawable.border0_listview);
	        	break;
	        case 1:vi.setBackgroundResource(R.drawable.border1_listview);
	        	break;
	        case 2:vi.setBackgroundResource(R.drawable.border2_listview);
	        	break;
	        case 3:vi.setBackgroundResource(R.drawable.border3_listview);
	        	break;
        }
        TextView sender = (TextView)vi.findViewById(R.id.sender); // sender
        TextView message_text = (TextView)vi.findViewById(R.id.message); // message body
        TextView date = (TextView)vi.findViewById(R.id.date); // timestamp
 
        HashMap<String, String> message = new HashMap<String, String>();
        message = data.get(position);
 
        // Setting all values in listview
        sender.setText(message.get(InboxFragment.KEY_EMAIL));
        message_text.setText(message.get(InboxFragment.KEY_TITLE));
        date.setText(message.get(InboxFragment.KEY_DATE));
	
		return vi;
    }
}