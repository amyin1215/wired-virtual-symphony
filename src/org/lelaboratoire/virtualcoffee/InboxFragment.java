package org.lelaboratoire.virtualcoffee;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.actionbarsherlock.app.SherlockFragment;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.SearchView.OnQueryTextListener;
import android.widget.TextView;

public class InboxFragment extends SherlockFragment {
	private static final String TAG = "Fragment Inbox Virtual Coffee";
    View view;
    ImageButton refresh;
    static final String KEY_ID = "id";
    static final String KEY_TITLE = "title"; 
    static final String KEY_EMAIL = "email"; 
    static final String KEY_DATE = "date";
    static final String KEY_MVMT0 = "mvmt0";
    static final String KEY_MVMT1 = "mvmt1";
    static final String KEY_MVMT2 = "mvmt2";
    static final String KEY_MVMT3 = "mvmt3";
 
    ListView list;
    InboxAdapter adapter;
    ArrayList<HashMap<String, String>> compositionsList;

    @Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
		Log.e(TAG, "onCreateView");
		// Inflate the layout for this fragment
		view = inflater.inflate(R.layout.inbox_list, container, false);
		this.setRefreshOnClick();
		new GetCompositionsTask().execute("");
		return view;
	}
    
    private void setRefreshOnClick() {
    	refresh = (ImageButton)view.findViewById(R.id.refresh);
        refresh.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
            	// get 10 newest compositions
            	new GetCompositionsTask().execute("");
            }
        });
    }
    
    class GetCompositionsTask extends AsyncTask<String, Void, JSONArray> {
	    @Override
	    protected JSONArray doInBackground(String... params) {
			/*if (!GCMRegistrar.isRegistered(getActivity())) {
				return null; 
			}
	    	String regid = GCMRegistrar.getRegistrationId(getActivity());*/
	    	return CoffeeServer.getCompositions("coffee"); 
	    }  
	    @Override
	    protected void onPostExecute(JSONArray jArray) {
			compositionsList = new ArrayList<HashMap<String, String>>();			
			
			// TODO: Figure out multi-registration for Paris vs. London phones
			/*if (!GCMRegistrar.isRegistered(getActivity())) {
				empty.setText("Please register this device under the Account Tab");
				// hide search bar and refresh button

				return; 
			}*/
			
			if (jArray == null) {
				hideCompositionWidgets();
			} else {
				showCompositionWidgets();
		    	for(int i=0;i<jArray.length();i++){
		        	JSONObject json_data;
					try {
						json_data = jArray.getJSONObject(i);
						String[] movements = {json_data.getString("mvmt0"),json_data.getString("mvmt1"),
						                      json_data.getString("mvmt2"),json_data.getString("mvmt3")};
			        	addComposition(""+json_data.getInt("composition_id"),
				            	json_data.getString("title"),
				            	json_data.getString("email"),
				            	movements, // needs to be symphony breakdown
				            	json_data.getString("date")
				            );
			        	Log.e("inbox fragment","json_data: " + json_data.toString());
					} catch (JSONException e) {
						e.printStackTrace();
						((TextView)view.findViewById(R.id.emptyTextView)).setText("Error finding compositions");
					}
		    	}
			}
	    	setAdapter();			
	    }
	}

    private void showCompositionWidgets() {
    	view.findViewById(R.id.emptyTextView).setVisibility(View.GONE);
    	refresh.setVisibility(View.VISIBLE);
    	SearchView search = (SearchView) view.findViewById(R.id.search);
    	search.setVisibility(View.VISIBLE);
    	search.setOnQueryTextListener(new OnQueryTextListener() {

			@Override
			public boolean onQueryTextChange(String arg0) {
				// TODO Auto-generated method stub
				return false;
			}

			@Override
			public boolean onQueryTextSubmit(String query) {
				new SearchCompositionsTask().execute(new String[]{"coffee", query});
				return false;
			}
    		
    	});
    }
    
    class SearchCompositionsTask extends AsyncTask<String, Void, JSONArray> {
	    @Override
	    protected JSONArray doInBackground(String... params) {
			/*if (!GCMRegistrar.isRegistered(getActivity())) {
				return null; 
			}
	    	String regid = GCMRegistrar.getRegistrationId(getActivity());*/
	    	return CoffeeServer.getCompositionsByEmail(params[0], params[1]); // palette, email
	    }  
	    @Override
	    protected void onPostExecute(JSONArray jArray) {
			compositionsList = new ArrayList<HashMap<String, String>>();			
			
			// TODO: Figure out multi-registration for Paris vs. London phones
			/*if (!GCMRegistrar.isRegistered(getActivity())) {
				empty.setText("Please register this device under the Account Tab");
				// hide search bar and refresh button

				return; 
			}*/
			
			// TODO: Figure out how to return to main list of symphonies after search
			if (jArray == null) {
				((TextView)view.findViewById(R.id.emptyTextView)).setText("No symphonies found for given email.");
			} else {
		    	for(int i=0;i<jArray.length();i++){
		        	JSONObject json_data;
					try {
						json_data = jArray.getJSONObject(i);
						String[] movements = {json_data.getString("mvmt0"),json_data.getString("mvmt1"),
						                      json_data.getString("mvmt2"),json_data.getString("mvmt3")};
			        	addComposition(""+json_data.getInt("composition_id"),
				            	json_data.getString("title"),
				            	json_data.getString("email"),
				            	movements, // needs to be symphony breakdown
				            	json_data.getString("date")
				            );
			        	Log.e("log_tag","json_data: " + json_data.toString());
					} catch (JSONException e) {
						e.printStackTrace();
						((TextView)view.findViewById(R.id.emptyTextView)).setText("Error finding compositions");
					}
		    	}
			}
	    	setAdapter();			
	    }
	}

    private void hideCompositionWidgets(){
    	refresh.setVisibility(View.GONE);
		view.findViewById(R.id.search).setVisibility(View.GONE);
		((TextView)view.findViewById(R.id.emptyTextView)).setText("No Symphonies to play");
    }

    private void addComposition(String composition_id, String title, 
    		String email, String[] movements, String date) {
        // creating new HashMap
        HashMap<String, String> map = new HashMap<String, String>();
        // adding each child node to HashMap key => value
        map.put(KEY_ID, composition_id);
        map.put(KEY_TITLE, title);
        map.put(KEY_EMAIL, email);
        map.put(KEY_MVMT0, movements[0]);
        map.put(KEY_MVMT1, movements[1]);
        map.put(KEY_MVMT2, movements[2]);
        map.put(KEY_MVMT3, movements[3]);
        map.put(KEY_DATE, date);

        // adding HashList to ArrayList
        compositionsList.add(map);	
    }
    
    private void setAdapter() {
        list=(ListView)view.findViewById(R.id.list);
 
        // Getting adapter by passing xml data ArrayList
        adapter = new InboxAdapter(getActivity(), compositionsList);
        list.setAdapter(adapter);
 
        // Click event for single list row
        list.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                    int pos, long id) {
            	// get composition info
            	HashMap<String, String> composition = (HashMap<String, String>) adapter.getItem(pos);
            	String title = composition.get(KEY_TITLE);
            	String email = composition.get(KEY_EMAIL);
            	String date = composition.get(KEY_DATE);
            	String[] movements = {composition.get(KEY_MVMT0),composition.get(KEY_MVMT1),composition.get(KEY_MVMT2),composition.get(KEY_MVMT3)};

            	((MainActivity) getActivity()).viewMessage(0, title, email, date, movements);
            }
        });
	}

}
