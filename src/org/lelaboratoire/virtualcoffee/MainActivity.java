package org.lelaboratoire.virtualcoffee;

import static org.lelaboratoire.virtualcoffee.CommonUtilities.DISPLAY_MESSAGE_ACTION;
import static org.lelaboratoire.virtualcoffee.CommonUtilities.EXTRA_MESSAGE;
import static org.lelaboratoire.virtualcoffee.CommonUtilities.SENDER_ID;
import static org.lelaboratoire.virtualcoffee.CommonUtilities.SERVER_URL;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragment;
import com.google.android.gcm.GCMRegistrar;
import java.io.IOException;
import org.lelaboratoire.BluetoothCoffee.BTClass;
import org.lelaboratoire.BluetoothCoffee.DeviceListActivity;
import org.lelaboratoire.BluetoothCoffee.OPhoneClass;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.IBinder;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.support.v4.app.FragmentTransaction;
import android.text.Html;
import android.util.Log;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends ParentTabsActivity{
	public static MusicService mServ;
	private static final String TAG = "Main Virtual Coffee";
	private static final boolean D = true;	
	private boolean mIsBound = false;
	public int track; // 0 - 4 scents; -1 is off
	OPhoneClass oPhone;
	BTClass bt = new BTClass();
	public static boolean isParis = true;
	public static boolean isCoffee = true;

    // Asyntask
    AsyncTask<Void, Void, Void> mRegisterTask;
 
    // Alert dialog manager
    AlertDialogManager alert = new AlertDialogManager();
 
    // Connection detector
    ConnectionDetector cd;
 
    public static String name = "Default Name";
    public static String email = "Default Email";
    
    // Intent request codes
    private static final int REQUEST_CONNECT_DEVICE = 1;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// get int URL that launched activity
		Uri data = getIntent().getData(); 
		if (data != null) {
			// if launched by URL, then switch to SingleScent tab
			int id = Integer.valueOf(data.getHost());
			getSupportActionBar().setSelectedNavigationItem(1);
			viewMessage(id, "sender", "message", "name", new String[]{"0001", "1001,0100", "0100,0000,1101", "1100"});			
		}
		doBindService();
		// Start music service
		Intent music = new Intent();
		music.setClass(this, MusicService.class);
		startService(music);
		
		// make oPhone object. All bt commands must use this bt instantiation
		oPhone = new OPhoneClass(bt);
		if (D)
			Log.e(TAG, "+++ ON CREATE +++");
		bt.checkAvailability(this);
				 
        // Check if Internet present
		cd = new ConnectionDetector(getApplicationContext());
        if (!cd.isConnectingToInternet()) {
            // Internet Connection is not present
            alert.showAlertDialog(MainActivity.this,
                    "Internet Connection Error",
                    "Please connect to working Internet connection", false);
            return;
        }
        // Getting name, email from intent
        Intent i = getIntent(); 
        if (i.hasCategory("Register")) {
        	Log.e(TAG, "Intent launched from Register"+i.toString());
            name = i.getStringExtra("name");
            email = i.getStringExtra("email");      
     
            // Make sure the device has the proper dependencies.
            GCMRegistrar.checkDevice(this);
            // Check that manifest is set
            GCMRegistrar.checkManifest(this);
     
            registerReceiver(mHandleMessageReceiver, new IntentFilter(
                    DISPLAY_MESSAGE_ACTION));
     
            // Get GCM registration id
            final String regId = GCMRegistrar.getRegistrationId(this);
     
            // Check if regid already presents
            if (regId.equals("")) {
                // Registration is not present, register now with GCM
                GCMRegistrar.register(this, SENDER_ID);
                Log.e(TAG, "Registration is not present; register now with GCM regId=" + regId);
            } else {
                // Device is already registered on GCM
                if (GCMRegistrar.isRegisteredOnServer(this)) {
                    // Skips registration.
                    Toast.makeText(getApplicationContext(), "Already registered with GCM", Toast.LENGTH_LONG).show();
                } else {
                    // Try to register again, but not in the UI thread.
                    // It's also necessary to cancel the thread onDestroy(),
                    // hence the use of AsyncTask instead of a raw thread.
                    final Context context = this;
                    mRegisterTask = new AsyncTask<Void, Void, Void>() {
                        @Override
                        protected Void doInBackground(Void... params) {
                            // Register on OPhone.fr server
                            Log.e(TAG, "doInBackground registration");
                            ServerUtilities.register(context, name, email, regId);
                            return null;
                        }
                        @Override
                        protected void onPostExecute(Void result) {
                            mRegisterTask = null;
                        }
                    };
                    mRegisterTask.execute(null, null, null);
                }
            }
        }

	}
	/* Menu with one option: Connect to OPhone via Bluetooth
	 */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
    	Log.e(TAG, "onCreateOptionsMenu");
        MenuInflater inflater = getSupportMenuInflater();
        inflater.inflate(R.menu.connect_option_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
    	Log.e(TAG, "onOptionsItemSelected");
        switch (item.getItemId()) {
	        case R.id.scan:
	            // Launch the DeviceListActivity to see devices and do scan
	            Intent serverIntent = new Intent(this, DeviceListActivity.class);
	            startActivityForResult(serverIntent, REQUEST_CONNECT_DEVICE);
	            return true;
        }
        return super.onOptionsItemSelected(item); 
    }
    
	
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(D) Log.d(TAG, "onActivityResult " + resultCode);
        switch (requestCode) {
        case BTClass.REQUEST_CONNECT_DEVICE:
            // When DeviceListActivity returns with a device to connect
            if (resultCode == Activity.RESULT_OK) {
                // Get the device MAC address
                String address = data.getExtras()
                                     .getString(DeviceListActivity.EXTRA_DEVICE_ADDRESS);               
                // Get the BluetoothDevice object
                bt.startConnection(address, this);
                status();              
            } else {
            	Toast.makeText(this,  "BT not connected", Toast.LENGTH_SHORT).show();
            }
            break;
        case BTClass.REQUEST_ENABLE_BT:
            // When the request to enable Bluetooth returns
            if (resultCode == Activity.RESULT_OK) {
                // Bluetooth is now enabled, so set up session
            	Toast.makeText(this, "Bluetooth successfully enabled", Toast.LENGTH_SHORT).show();
            	BTClass.btEnabled = true;
            	BTClass.btConnected = true;
            	if (D) Log.e(TAG, "btEnabled");
            } else {
                // User did not enable Bluetooth or an error occured
                if (D) Log.d(TAG, "BT not enabled");
                Toast.makeText(this, "Bluetooth was not enabled", Toast.LENGTH_SHORT).show();
                finish();
            }
        }
    }
	
	/**
     * Receiving push messages
     * */
    private final BroadcastReceiver mHandleMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String newMessage = intent.getExtras().getString(EXTRA_MESSAGE);
            // Waking up mobile if it is sleeping
            WakeLocker.acquire(getApplicationContext());
 
            /**
             * Take appropriate action on this message
             * depending upon your app requirement
             * TODO: Go to inbox
             * */
 
            // Showing received message
            Toast.makeText(getApplicationContext(), "New Symphony: " + newMessage, Toast.LENGTH_LONG).show();
            ActionBar actionBar = getSupportActionBar();
            actionBar.setSelectedNavigationItem(2);
 
            // Releasing wake lock
            WakeLocker.release();
        }
    };
 
	public void release(View view) {
		if (!bt.btConnected)
			Toast.makeText(this, "Please connect to bluetooth. Select Options Menu->Connect",Toast.LENGTH_SHORT);
		int tag = Integer.valueOf(view.getTag().toString());
		// if the scent selected has a track currently playing
		if (tag == track) {
			oPhone.scheduleScentsOff(new int[]{tag}, 0);
			Log.e(TAG, "pause music track: " + track);
			mServ.pauseMusic();
			track = -1;
		}
		// if track is off or if new scenttrack is selected
		else {
			// turn off existing fans
			Log.e(TAG, "release's tag: " + tag);
			Log.e(TAG, "resume music track: " + track);
			// set music to current view's corresponding track
			track = Integer.valueOf((String) view.getTag());
			if (track >= 0 && track <4) {
				try {
					// if another scent is already on, turns old off and new on
					oPhone.scentOn(tag);
					mServ.resumeMusic(track, this);
				} catch (IllegalStateException e) {
					mServ.pauseMusic();
					oPhone.scheduleScentsOff(new int[]{tag}, 0);
					e.printStackTrace();
				} catch (IOException e) {
					track = 0;
					e.printStackTrace();
				}
			}
		}
	}
	
	private ServiceConnection Scon = new ServiceConnection()
	{
		@Override
		public void onServiceConnected(ComponentName name, IBinder binder) 
		{
			mServ = ((MusicService.ServiceBinder)binder).getService();
		}

		@Override
		public void onServiceDisconnected(ComponentName name) 
		{
			mServ = null;
		}
	};

	void doBindService(){
		Log.e(TAG, "doBindService called");
 		bindService(new Intent(this,MusicService.class),
				Scon,Context.BIND_AUTO_CREATE);
		mIsBound = true;
	}

	void doUnbindService()
	{
		if(mIsBound)
		{
			unbindService(Scon);
      		mIsBound = false;
		}
	}

	@Override
	public void onDestroy() {
		Log.e(TAG, "Destroy");
        if (mRegisterTask != null) {
            mRegisterTask.cancel(true);
        }
        try {
            unregisterReceiver(mHandleMessageReceiver);
            GCMRegistrar.onDestroy(this);
        } catch (Exception e) {
            Log.e("UnRegister Receiver Error", "> " + e.getMessage());
        }
		doUnbindService();
		super.onDestroy();
	}

	
	// Start intra-ophone message dialog when envelope is pressed
	public void composeOMessage(int scentid) {
		Intent oMessageIntent = new Intent(MainActivity.this, OMessageActivity.class);
		oMessageIntent.putExtra("scentid", scentid);
		MainActivity.this.startActivity(oMessageIntent);
	}
	
	public void viewMessage(int id, String sender, String message, String name, String[] movements) {
		FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
		ft.setCustomAnimations(R.anim.push_left_in, R.anim.push_left_out);
		ViewMessageFragment newFragment = 
				ViewMessageFragment.newInstance(id, movements, sender, message, name);
        SherlockFragment preInitializedFragment = (SherlockFragment) getSupportFragmentManager().findFragmentByTag("Inbox");

        if (preInitializedFragment != null) {
            ft.detach(preInitializedFragment);
        }
        
        ft.replace(android.R.id.content, newFragment, "ViewMessage");
		ft.addToBackStack("Inbox");
		ft.commit();
	}
	
	public void playComposition(int id, String[] mvmts, String title, String email, String date) {
		FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
		ft.setCustomAnimations(R.anim.push_left_in, R.anim.push_left_out);

		ViewMessageFragment newFragment = 
				ViewMessageFragment.newInstance(id, mvmts, title, email, date);
        SherlockFragment preInitializedFragment = (SherlockFragment) getSupportFragmentManager().findFragmentByTag("Inbox");

        if (preInitializedFragment != null) {
            ft.detach(preInitializedFragment);
        }
        
        ft.replace(android.R.id.content, newFragment, "ViewMessage");
		ft.addToBackStack("Inbox");
		ft.commit();
	}

	@Override
	public void onPause() {
		super.onPause();
		if (D)
			Log.e(TAG, "- ON PAUSE -");		
		bt.pauseConnection(oPhone);
		Toast.makeText(this, "Bluetooth pause", Toast.LENGTH_SHORT).show();
	}
	
    private void status(){
		if (D)
			Log.e(TAG, "+ ABOUT TO PING SERVER WITH STATUS INQUIRY +");
		
		if (bt.sendDataByte((byte)(OPhoneClass.OPHONE + OPhoneClass.STATUS))) {
			byte[] msg = null;
			BTClass.btConnected = true;
			if (null != (msg = bt.receiveData())) {
				Toast.makeText(this, "Successful connection to OPhone!", Toast.LENGTH_SHORT).show();
			} else {
				Toast.makeText(this,  "No data received during initial ping", Toast.LENGTH_SHORT).show();
				Log.e(TAG, "no data in handshake");
			}
		} else {
			Log.e(TAG, "sendData returned false");
			Toast.makeText(this,  "Unable to initiate BT connection to OPhone during Handshake", Toast.LENGTH_SHORT).show();
			BTClass.btConnected = false;
		}
    }
}
