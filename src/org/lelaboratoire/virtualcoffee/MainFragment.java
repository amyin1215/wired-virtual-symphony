package org.lelaboratoire.virtualcoffee;

import com.actionbarsherlock.app.SherlockFragment;
import android.os.Bundle;
import android.annotation.SuppressLint;
import android.content.ClipData;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.text.Html;
import android.util.Log;
import android.view.DragEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.View.DragShadowBuilder;
import android.view.View.OnDragListener;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class MainFragment extends SherlockFragment {
	private static final String TAG = "Fragment Main Virtual Coffee";
    View view;

    @Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
		Log.e(TAG, "onCreateView");
		// Inflate the layout for this fragment
		view = inflater.inflate(R.layout.activity_about, container, false);
		locationSetup();
		setBackgroundOpacity();
		return view;
	}
	
	private void locationSetup() {
		
		Resources res = getActivity().getResources();
		String toLocation = res.getString(R.string.paris);
		if (MainActivity.isParis) {
			toLocation = res.getString(R.string.london);
		}
		Log.e(TAG, toLocation);
		String text = String.format(res.getString(R.string.about), toLocation);
		CharSequence styledText = Html.fromHtml(text);
		((TextView)view.findViewById(R.id.about_text)).setText(styledText);
	}

	private void setBackgroundOpacity() {
		View backgroundimage = view.findViewById(R.id.ophone_background);
		Drawable background = backgroundimage.getBackground();
		background.setAlpha(80);
	}
}
