package org.lelaboratoire.virtualcoffee;

import java.io.IOException;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnErrorListener;
import android.net.Uri;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

// Code borrowed from http://www.codeproject.com/Articles/258176/Adding-Background-Music-to-Android-App
public class MusicService extends Service  implements MediaPlayer.OnErrorListener{

    private final IBinder mBinder = new ServiceBinder();
    MediaPlayer mPlayer;
    private int length = 0;
	int music[] = new int[]{R.raw.lisanga,R.raw.coldplay_trouble, 
			R.raw.hawaii_chocolate, R.raw.vicarious_life};

    public MusicService() { }

    public class ServiceBinder extends Binder {
     	 MusicService getService()
    	 {
    		return MusicService.this;
    	 }
    }

    @Override
    public IBinder onBind(Intent arg0){return mBinder;}

    @Override
    public void onCreate (){
    	super.onCreate();
	  	
    	Log.e("MusicService", "IBinder OnCreate");
    	mPlayer = MediaPlayer.create(this, R.raw.vicarious_life);
    	mPlayer.setOnErrorListener(this);

    	if(mPlayer!= null)
        {
    		mPlayer.setLooping(true);
    		mPlayer.setVolume(100,100);
        }

        mPlayer.setOnErrorListener(new OnErrorListener() {

        	public boolean onError(MediaPlayer mp, int what, int extra){
        		onError(mPlayer, what, extra);
        		return true;
        	}
        });
	}

    @Override
	public int onStartCommand (Intent intent, int flags, int startId)
	{
         mPlayer.start();
         Log.e("MusicService", "onStart after mPlayer.start");
         mPlayer.pause();
         return START_STICKY;
	}

	public void pauseMusic()
	{
		if(mPlayer.isPlaying())
		{
			mPlayer.pause();
			length=mPlayer.getCurrentPosition();

		}
	}

	public void resumeMusic(int track, Context context) throws IllegalStateException, IOException
	{
		mPlayer.reset(); // reset to uninitialized state
		mPlayer.setDataSource(context, 
				Uri.parse("android.resource://org.lelaboratoire.virtualcoffee/" + music[track]));

		mPlayer.prepare(); // bring to idle state
		mPlayer.start();

	}

	public void stopMusic()
	{
		mPlayer.stop();
		mPlayer = null;
	}

	@Override
	public void onDestroy ()
	{
		super.onDestroy();
		if(mPlayer != null)
		{
		try{
		 mPlayer.stop();
		 mPlayer.release();
			}finally {
				mPlayer = null;
			}
		}
	}

	public boolean onError(MediaPlayer mp, int what, int extra) {

		Toast.makeText(this, "music player failed", Toast.LENGTH_SHORT).show();
		if(mPlayer != null)
		{
			try{
				mPlayer.stop();
				mPlayer.release();
			}finally {
				mPlayer = null;
			}
		}
		return false;
	}
}
