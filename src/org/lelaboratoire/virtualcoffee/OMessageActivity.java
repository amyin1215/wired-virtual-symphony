package org.lelaboratoire.virtualcoffee;

import static org.lelaboratoire.virtualcoffee.CommonUtilities.SERVER_URL;

import java.util.ArrayList;
import java.util.Arrays;

import org.lelaboratoire.aesthetics.TypefaceSpan;

import com.google.android.gcm.GCMRegistrar;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.text.Editable;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableString;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.support.v4.app.FragmentActivity;

public class OMessageActivity extends FragmentActivity implements OnClickListener{
	Button sendbtn;
	public static int receiver_id = 0;
	public String message = "Default message";
	public static RecipientsSpinnerAdapter adapter;
	public static User[] users;
	public int scentid;
	static ArrayAdapter<CharSequence> scentsAdapter;
	private final boolean D = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setTitle(setFont("Send an ONOTE"));
		Intent intent = getIntent();
		scentid = intent.getIntExtra("scentid", 0);
		setContentView(R.layout.activity_omessage);
	    sendbtn = (Button) findViewById(R.id.send_btn);
	    sendbtn.setOnClickListener(this);
	    EditText message_input = (EditText)findViewById(R.id.message_input);
	    message_input.setOnClickListener(this);
	    // dismiss keyboard by touching outside edittext box
	    LinearLayout layout = (LinearLayout)findViewById(R.id.layout);
	    layout.setOnClickListener(this);
		ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
        if (!cd.isConnectingToInternet()) {
            // Internet Connection is not present
            Toast.makeText(getApplicationContext(), "Please connect to internet!", Toast.LENGTH_SHORT).show();
            User users[] = new User[10];
            for (int i=1; i<=10; i++) {
                users[i-1] = new User();
                users[i-1].setName("Phone " + i);
                users[i-1].setId(i-1);
            }            
            setRecipientsSpinner(users);
        } else {
        	// get recipients from database
            new PopulateRecipientsTask().execute(""); 
        }
	     
        // Populate scents spinner
	    setScentsSpinner();
	}
	class PopulateRecipientsTask extends AsyncTask<String, User[], User[]> {
	    @Override
	    protected User[] doInBackground(String... params) {
	    	return CoffeeServer.populateRecipients();
	    }  
	    @Override
	    protected void onPostExecute(User[] users) {
	    	setRecipientsSpinner(users);
	    }
	}
	
	private void setRecipientsSpinner(User[] users) {
		ArrayList<User> userList = new ArrayList<User>(Arrays.asList(users));
		User addEmail = new User();
		addEmail.setName("Send to email");
		addEmail.setId(0);
		userList.add(addEmail); 

		// add "enter email address" option
        adapter = new RecipientsSpinnerAdapter(this,
	            android.R.layout.simple_spinner_item,
	            userList);
        Spinner recipientsSpinner = (Spinner) findViewById(R.id.recipients_spinner);
        recipientsSpinner.setAdapter(adapter); // Set the custom adapter to the spinner

        recipientsSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view,
                    int pos, long id) {
                // Here you get the current item (a User object) that is selected by its position
                User user = adapter.getItem(pos);
                OMessageActivity.receiver_id = user.getId();
                // if the id is 0, enable EditText to input email address
                int emailField = (OMessageActivity.receiver_id == 0) ? View.VISIBLE : View.GONE;
                ((EditText)findViewById(R.id.EmailInput)).setVisibility(emailField);
               	((TextView)findViewById(R.id.EmailText)).setVisibility(emailField);
               
                Log.e("OMessage", "Selected: " + pos + " id: +" + id+2);
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapter) {  }
        });
	}
	
	private void setScentsSpinner() {
        Spinner scentsSpinner = (Spinner) findViewById(R.id.scents_spinner);
        scentsAdapter = ArrayAdapter.createFromResource(this,
                R.array.scents_array, R.layout.spinner);
        scentsAdapter.setDropDownViewResource(R.layout.spinner);
        scentsSpinner.setAdapter(scentsAdapter);
        
        scentsSpinner.setSelection(scentid);
        scentsSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view,
                    int pos, long id) {
                // Here you get the current item (a User object) that is selected by its position
                scentid = pos;
                Log.e("OMessage", "Selected scent id: " + pos);
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapter) {  }
        });
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()){
		case R.id.message_input:
			// do nothing if edittext box clicked
			break;
		case R.id.send_btn:
			EditText msg = (EditText) findViewById(R.id.message_input);
			message = msg.getText().toString();
			new SendScentmailTask().execute("");
			finish(); // dismiss dialog
			break;
		case R.id.layout:
			hideSoftKeyboard(OMessageActivity.this);
		}
	}
	
	private class SendScentmailTask extends AsyncTask<String, Void, String> {
		@Override
		protected String doInBackground(String... params) {
			if (receiver_id > 0) {
				// send message to another ophone, logging message into database
				CoffeeServer.sendScentmail(message,scentid, receiver_id, GCMRegistrar.getRegistrationId(getApplicationContext()));
			} else {
				// send email instead 
				String[] emailAddress = new String[] {((EditText)findViewById(R.id.EmailInput)).getText().toString()};
				composeEmail(message, scentid, emailAddress);
			}
				
			return null;
		}
	}
 
	public void composeEmail(String message, int scentid, String[] email) {
		String scentName = getResources().getStringArray(R.array.scents_array)[scentid];
		String body = new StringBuilder().append("</a>")
				.append("Message: " + message + "<br>")
				.append(scentName +" coffee, across space and time.")
				.append("<br>")
				.append("<br><a href = \""+SERVER_URL+"/view_message.php/"+scentid+"/sender/message")
				.append(scentid +"\">vcoffee://" + scentid +"</a><br><br>")
				.append("If you cannot open the link, you may need to download the VirtualCoffee app for OPhone. ")
				.append("Otherwise, try copying and pasting the link into your phone's browser.")
				.toString();
		Intent emailIntent = new Intent();
		emailIntent.setAction(Intent.ACTION_SEND);
		if (D) email = new String[]{"amymichelleyin@gmail.com"};
		emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, email);
		emailIntent.putExtra(Intent.EXTRA_SUBJECT, "LaboCafe: Virtual Coffee on your oPhone");
		emailIntent.putExtra(Intent.EXTRA_TEXT, Html.fromHtml(body));
		emailIntent.setType("text/html");
		startActivity(Intent.createChooser(emailIntent, "Send " + scentName));
	}
	
    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager = (InputMethodManager)  activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
    }
    
	private SpannableString setFont(String s){
		SpannableString mSS = new SpannableString(s);
		mSS.setSpan(new TypefaceSpan(this), 0, s.length(),
		        Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
		return mSS;
	}
}
