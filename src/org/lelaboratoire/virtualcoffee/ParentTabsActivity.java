package org.lelaboratoire.virtualcoffee;

import org.lelaboratoire.aesthetics.TypefaceSpan;

import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.util.Log;
import android.view.ViewGroup;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.ActionBar.Tab;
import com.actionbarsherlock.app.SherlockFragmentActivity;

public abstract class ParentTabsActivity extends SherlockFragmentActivity {
	int position;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		// Determine which bundle to use; either the saved instance or a bundle
	    // that has been passed in through an intent.
	    Bundle bundle = getIntent().getExtras();
	    if (bundle == null) {
	        bundle = savedInstanceState;
	    }

	    // Initialize members with bundle or default values.
	    if (bundle != null) {
	        position = bundle.getInt("selected_tab");
	    } else {
	        position = 0;
	    }
	    
	    actionbarSetup();
	}
	private void actionbarSetup(){
		boolean[] selected = new boolean[3];
		selected[position] = true;
		Log.e("ParentTabs", "position" + position);
		ActionBar bar = getSupportActionBar();
		bar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
		

		bar.setTitle(setFont("OTRACKS"));
		    
		
		bar.addTab(bar
				.newTab()
	            .setText(setFont("About"))
	            .setTabListener(new TabsListener<MainFragment>(
	            		this, "Main", MainFragment.class)),
	                    0, selected[0]);
						
		bar.addTab(bar
	            .newTab()
	            .setText(setFont("Compositions"))
	            .setTabListener(new TabsListener<InboxFragment>(
	                    this, "Inbox", InboxFragment.class)),
	                    1, selected[1]);

		 bar.setSelectedNavigationItem(position);
	}
	
	private SpannableString setFont(String s){
		SpannableString mSS = new SpannableString(s);
		mSS.setSpan(new TypefaceSpan(this), 0, s.length(),
		        Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
		return mSS;
	}
	
}
