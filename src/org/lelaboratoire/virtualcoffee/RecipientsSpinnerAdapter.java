package org.lelaboratoire.virtualcoffee;

import java.util.ArrayList;

import org.lelaboratoire.aesthetics.TypefaceSpan;

import android.app.Activity;
import android.graphics.Typeface;
import android.text.Spannable;
import android.text.SpannableString;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class RecipientsSpinnerAdapter extends ArrayAdapter<User>{
    private Activity context;
    private ArrayList<User> values;

    public RecipientsSpinnerAdapter(Activity context, int textViewResourceId,
            ArrayList<User> values) {
        super(context, textViewResourceId, values);
        this.context = context;
        this.values = values;
    }

    public int getCount(){
       return values.size();
    }

    public User getItem(int position){
       return values.get(position);
    }

    public long getItemId(int position){
       return values.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
    	org.lelaboratoire.aesthetics.MyTextView textView = (org.lelaboratoire.aesthetics.MyTextView) View.inflate(context, R.layout.spinner, null);
    	//org.lelaboratoire.aesthetics.MyTextView textView = new org.lelaboratoire.aesthetics.MyTextView(context);
        textView.setText(values.get(position).getName());
        textView.setPadding(15, 15, 15, 15);
        return textView;
    }

    @Override
    public View getDropDownView(int position, View convertView,
            ViewGroup parent) {
    	org.lelaboratoire.aesthetics.MyTextView textView = (org.lelaboratoire.aesthetics.MyTextView) View.inflate(context, R.layout.spinner, null);
        textView.setText(values.get(position).getName());
        return textView;
    }
}