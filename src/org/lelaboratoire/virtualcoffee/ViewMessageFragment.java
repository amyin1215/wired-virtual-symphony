package org.lelaboratoire.virtualcoffee;

import java.io.IOException;
import java.util.Dictionary;
import java.util.Hashtable;
import java.util.Timer;
import java.util.TimerTask;

import org.lelaboratoire.BluetoothCoffee.OPhoneClass;

import com.actionbarsherlock.app.SherlockFragment;

import android.os.AsyncTask;
import android.os.Bundle;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class ViewMessageFragment extends SherlockFragment {
	private static final String TAG = "Single Scent vCoffee";
	private int track = -1;
	private static int id;
	private String title; // message
	private String email; // email
	private String date; // name
	private String[] mvmts = new String[CoffeeServer.MAX_MVMTS];
	boolean isPlaying = false;
	private String current_mvmt = "";
	Timer mvmts_timer;
	View view;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
		Log.e(TAG, "onCreateView");
		id = getArguments().getInt("id",0);
		mvmts = getArguments().getStringArray("mvmts");
		title = getArguments().getString("title");
		email = getArguments().getString("email");
		date = getArguments().getString("date");
		// Inflate the layout for this fragment
		view = inflater.inflate(R.layout.view_composition, container, false);
		setup(view);
		Log.e(TAG, "oncreate = " + mvmts[0] + mvmts[1] + mvmts[2] + mvmts[3]);

		return view;
	}
	
	public static ViewMessageFragment newInstance(int id, String[] mvmts, String title, String email, String date) {
		ViewMessageFragment myFragment = new ViewMessageFragment ();
		Log.e(TAG, "mvmts = " + mvmts[0] + mvmts[1] + mvmts[2] + mvmts[3]);
	    Bundle args = new Bundle();
	    args.putStringArray("mvmts", mvmts);
	    args.putString("title", title);
	    args.putString("email", email);
	    args.putString("date", date);
	    myFragment.setArguments(args);

	    return myFragment;
	}

	private void setup(View view) {
		TextView senderView = (TextView) view.findViewById(R.id.sender);
		TextView messageView = (TextView) view.findViewById(R.id.message);
		
		String escapedUsername = TextUtils.htmlEncode(date);
		Resources res = getResources();
		String text = String.format(res.getString(R.string.message), escapedUsername);
		CharSequence styledText = Html.fromHtml(text);
		
		senderView.setText(styledText);
		messageView.setText(title);
		
		setMeasureColors();
		
		Button background = (Button)view.findViewById(R.id.background);
		background.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				scentRelease(v);
				startComposition(v);
			}
		});
	}

	public void startComposition(View play_button) {
		// if track is on
		Log.e(TAG, "release");
		// pause if track is currently playing
		if (isPlaying) {
			((MainActivity)getActivity()).oPhone.scheduleScentsOff(mvmtStringToScentCommands(current_mvmt), 0);
			mvmts_timer.cancel();
		} else {
			Log.e(TAG, "resume music track: " + track + "; id= " + id);
			isPlaying = true;
			// schedule timers to offset four movements
			mvmts_timer = new Timer();
			int move_time = 0; // milliseconds given for each motor to move
			for (String mvmt : mvmts) {
				mvmts_timer.schedule(new ScheduleMovementReleaseTask(mvmt), move_time);
				move_time += OPhoneClass.MVMT_LENGTH;
			}
			// start staff animation
        	LinearLayout staff = (LinearLayout) getView().findViewById(R.id.staff);
        	Animation move_right = AnimationUtils.loadAnimation(getActivity(), R.anim.measure_animation);
        	staff.startAnimation(move_right);
        	move_right.setAnimationListener(new AnimationListener() {
				@Override
				public void onAnimationEnd(Animation arg0) {
					isPlaying = false;
				}
				@Override
				public void onAnimationRepeat(Animation arg0) {}
				@Override
				public void onAnimationStart(Animation arg0) {}
        		
        	});
        	// shrink play button
        	Button play = (Button)getView().findViewById(R.id.background);
        	Animation shrink_button = AnimationUtils.loadAnimation(getActivity(), R.anim.shrink_play);
        	play.startAnimation(shrink_button);
		}
	}

	private void setMeasureColors() {
		for (int mvmtID = 0; mvmtID < CoffeeServer.MAX_MVMTS; mvmtID++) {
			String[] scents = mvmts[mvmtID].split(",");
			// dictionary of category as key, selection as value
			Dictionary<Integer, Integer> selections = new Hashtable<Integer,Integer>();
			for (int jj = 0; jj < scents.length; jj++) {
				int category = Integer.parseInt(scents[jj].substring(0,2), 2); // first two letters of scents
				int selection = Integer.parseInt(scents[jj].substring(2,4), 2); // last two letters of scents
				selections.put(category, selection);
			}
			for (int kk = 0; kk < 4; kk++) {
				Integer selection = selections.get(kk);
				if (selection != null) {
					setMeasureColor(mvmtID, kk, selection);
				} else {
					setMeasureColor(mvmtID, kk, -1);
				}
			}
		}
	}
	
	private void setMeasureColor(int mvmtID, int category, int selection) {
		LinearLayout measure;
		switch(mvmtID){
			case 0: measure = (LinearLayout)view.findViewById(R.id.mvmt0);
				break;
			case 1: measure = (LinearLayout)view.findViewById(R.id.mvmt1);
				break;
			case 2: measure = (LinearLayout)view.findViewById(R.id.mvmt2);
			 	break;
			case 3: measure = (LinearLayout)view.findViewById(R.id.mvmt3);
				break;
			default: 
				measure = (LinearLayout)view.findViewById(R.id.mvmt0);
		}
		
		// need to set measure to linearlayout measure based on mvmtID
		ImageView categoryView = (ImageView)measure.getChildAt(category);
		GradientDrawable shapeDrawable = (GradientDrawable)categoryView.getDrawable();
		Resources res = getActivity().getResources();
		// get color for that category and that selection
		TypedArray categoryArrays = res.obtainTypedArray(R.array.coffee_palette_color_arrays);
		TypedArray selections = res.obtainTypedArray(categoryArrays.getResourceId(category, R.array.coffee_colors));

		int empty_gray = res.getColor(R.color.empty_measure);
		int color = (selection == -1) ? empty_gray : selections.getColor(selection, empty_gray);
		shapeDrawable.setColor(color);
		selections.recycle();
		categoryArrays.recycle();
		
	}
	
   // move motor
   class ScheduleMovementReleaseTask extends TimerTask {
	   String mvmt; 
	   
	   public ScheduleMovementReleaseTask(String mvmt) {
		   this.mvmt = mvmt;
	   }
        @Override
        public void run() {
        	Log.e("ViewMessage", "Movement " + mvmt + "is scheduled!");
        	((MainActivity)getActivity()).oPhone.scentsOn(mvmtStringToScentCommands(mvmt));
        	current_mvmt = mvmt;
        }
   };
	
	/*
	 * Take string such as "0001,1001" and convert to {1,9}
	 */
	private int[] mvmtStringToScentCommands(String mvmt) {
		int[] scent_commands = new int[4];
		// parse until comma reached
		String[] binaries = mvmt.split("[,]");
		// if the string has more than three commas, disregard end
		int len = binaries.length > 4 ? 4 : binaries.length;
		for (int ii = 0; ii < len; ii++) {
			scent_commands[ii] = Integer.parseInt(binaries[ii]);
		}
		return scent_commands;
	}
	
	public void scentRelease(View view) {
		// if track is on
		Log.e(TAG, "scent release");
		// if track is currently playing
		if (track >= 0 && track < 4) {
			//MainActivity.mServ.pauseMusic();
			track = -1;
			((MainActivity)getActivity()).oPhone.scheduleScentsOff(new int[]{id}, 0);
			
		} else {
			Log.e(TAG, "resume track: " + track + "; id= " + id);
			track = id;
			((MainActivity)getActivity()).oPhone.scentOn(id);
		}
	}
	
	@Override
	public void onPause() {
		super.onPause();
	}
}
